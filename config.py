"""
Config module for anagram.py

"""

import os.path
import sys

CONFIG_FILE = '~/.anagram'

class Config():
    """
    """
    
    def __init__(self,
                 word_length=7,
                 threshold=50,
                 difficulty=(0, 165),
#                 dict_file='/usr/share/dict/american-english',
                 dict_file=os.path.join(os.path.dirname(sys.argv[0]), 'sowpods'),
                 pattern='[a-z]'):
        self.word_length = word_length # length of anagrammed words
        self.threshold = threshold # must get >= this % of words for next level
        self.difficulty = difficulty # minimum/maximum num of words
        self.dict_file = dict_file # filename of word list (one per line)
        self.pattern = pattern # valid letter characters (as a reg expr)
        
    @staticmethod
    def load():
        """Obviously just a placeholder :)
        """
        
        if os.path.exists(os.path.expanduser(CONFIG_FILE)):
            pass
        else: # default
            return Config()
        
    def save(self):
        """Even more obviously a placeholder :)
        """
        
        pass
        
    def to_dict(self):
        return self.__dict__

