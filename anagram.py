#!/usr/bin/env python3
"""
A game where you make as many words as you can with the given letters.

This is a game like this one we used to have on Windows, but I can't remember
the name of it.  It starts with a word (7-8 letters, usually) and scrambles the
letters.  There's a list of the possible words (but it's all empty underscores
at the beginning) and the letters to use.  Under that is the space for making
words.  You type or click on the letters to queue them in the space, and you
hit enter or click a submit-type button to try that word.  If correct, it's
added to the list above.  When you've gotten a certain percentage correct, you
can move to the next word.  There's a timer, too, I think.
"""

import pygame

from constants import *
from level import *
from menu import *
from words import *

def main():
    # Pygame initialization:
    pygame.init()
    pygame.font.init()
    
    screen = pygame.display.set_mode(SCREEN_SIZE)
    
    # Load images & words:
    background = pygame.Surface(SCREEN_SIZE).convert()
    background.fill(BG_COLOR)
    
    letterbg = pygame.Surface(LETTER_SQUARE_SIZE).convert()
    letterbg.fill(LETTER_BG_COLOR)
    
    submitbg = pygame.Surface((LETTER_SQUARE_SIZE[0] * 2,
                               LETTER_SQUARE_SIZE[0])).convert()
    submitbg.fill(LETTER_BG_COLOR)
    
    bgs = {"background": background, "letterbg": letterbg, "submitbg": submitbg}
    
    bigfont = pygame.font.Font(None, LETTER_SIZE)
    medfont = pygame.font.Font(None, MEDIUM_LETTER_SIZE)
    
    fonts = {"bigfont": bigfont, "medfont": medfont}
    
    config = None
    
    def new_word_generator(used=None):
        if used is None:
            used = []
        count = 1 # DEBUG
        while True:
            print(count) # DEBUG
            count += 1 # DEBUG
            for w, w_len in words.word_lens.items():
                if w not in used and w_len in range(*config.difficulty):
                    print(words.word_lens) # DEBUG
                    print((w not in used), used) # DEBUG
                    used.append(w)
                    print((w not in used), used) # DEBUG
                    yield (w, w_len)
            words.add_word()

    def new_word(nwg = new_word_generator()):
        return next(nwg)
    
    while True: # MENU
        config = menu(screen, config)
        words = Words(config)
        
        quit = False
        
        while not quit: # GAME
            screen.blit(bgs["background"], (0, 0))
            pygame.display.flip()
            
            level = Level(screen, new_word(), config, bgs, fonts)
            quit = level.play()
            
if __name__ == '__main__':
    main()
