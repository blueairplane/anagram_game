Anagram Game
============

A (rather basic) game where you try to find all the words you can make using the letters in a long-ish (currently 7-letter) word.  You can move on to a new word after meeting a threshold of found words.

Requirements:
* Python 3
* pygame
* an
* grep
