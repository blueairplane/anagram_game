"""
Menu module for anagram.py

"""

import sys

import pygame

from buttons import *
from config import *
from constants import *

#def load_config():
#    """Obviously just a placeholder :)
#    """
#    return Config(difficulty=(0,250))

def menu(surf, config):
    """
    """
    
    offset = 5 # amt min and max change
    
    if not config:
        config = Config.load()
    
    font = pygame.font.Font(None, LETTER_SIZE)
    button_bg = pygame.Surface(WIDE_BUTTON_SIZE).convert()
    button_bg.fill(LETTER_BG_COLOR)
    
    col = (
        surf.get_width() // 2,
    )
    row = (
        surf.get_height() * 1 // 3,
        surf.get_height() * 1 // 3 + LETTER_SIZE * 3 // 2,
        surf.get_height() * 1 // 3 + LETTER_SIZE * 3,
        surf.get_height() * 1 // 3 + LETTER_SIZE * 9 // 2
    )
    
    difficulty_label = Label('Select difficulty level', LETTER_SIZE, LETTER_COLOR, BG_COLOR)
    difficulty_label.rect.center = (col[0], row[0])
    
    difficulty_buttons = {
        'easy': TextButton('Easy', LETTER_SIZE, button_bg),
        'medium': TextButton('Medium', LETTER_SIZE, button_bg),
        'hard': TextButton('Hard', LETTER_SIZE, button_bg)
    }
    for n, button in enumerate(difficulty_buttons.values(), start=1):
        button.rect.center = (col[0], row[n])
    
    buttons = pygame.sprite.RenderPlain()
    buttons.add(difficulty_buttons.values())
    # not really buttons, but we're going to combine these parts into one
    # class soon, anyway
    buttons.add(difficulty_label)
    
    surf.fill(BG_COLOR)
    buttons.draw(surf)
    
    pygame.event.set_allowed(None)
    pygame.event.set_allowed((pygame.QUIT, pygame.MOUSEBUTTONDOWN))
    # ~ pygame.display.flip()
    minmax_text_size = font.render('0000', False, (0,0,0))
    
    while True:
        pygame.display.flip()
        events = [pygame.event.wait()] + pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if difficulty_buttons['easy'].rect.collidepoint(pos):
                    config.difficulty = (0, 50)
                    return config
                elif difficulty_buttons['medium'].rect.collidepoint(pos):
                    config.difficulty = (50, 100)
                    return config
                elif difficulty_buttons['hard'].rect.collidepoint(pos):
                    config.difficulty = (100, 165)
                    return config
                        
                ### NEED TO ADD HERE, OBVIOUSLY
                ### ALSO NEED TEXT LABELS AND BUTTON OBJS
                ### TO VIEW / CHANGE SETTINGS


#class ValueChangeButton(pygame.sprite.Sprite):
#    """
#    """
#    
#    def __init__(self, label, start_value, (x, y), (width, height)):
#        pygame.sprite.Sprite.__init__(self)
#        
#        font = pygame.font.Font(None, LETTER_SIZE)
#        button_font = pygame.font.Font(None, SMALL_LETTER_SIZE)
#        button_bg = pygame.Surface(SMALL_LETTER_SQUARE_SIZE).convert()
#        button_bg.fill(LETTER_BG_COLOR)
#        
#        self.label = font.render(label, True, LETTER_COLOR).convert()
#        self.value = start_value
#        self.rect = pygame.Rect(x, y, width, height)
#        
#        self.image = pygame.Surface((x, y)).convert()
#        self.image.fill(BG_COLOR)
#        self.image.blit(self.label, (0, 0))
#        self.buttons = {'more': TextButton('+', self.button_font,
#                                          self.button_bg),
#                        'less': TextButton('-', self.button_font,
#                                          self.button_bg),}
#        self.buttons['more'].rect.topright = self.rect.topright
#        self.buttons['less'].rect.bottomright = self.rect.bottomright
#        self.image.blit(self.buttons['more'].image, self.buttons['more'].rect)
#        self.image.blit(self.buttons['less'].image, self.buttons['less'].rect)
