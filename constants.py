"""
Global definitions for anagram.py

"""

from collections import namedtuple
from itertools import dropwhile
from math import isclose

from pygame import display, color


Screen_size = namedtuple('Screen_size', 'width height')
display.init()
SCREEN_SIZE = next(dropwhile(lambda x: not isclose(x[0]/x[1], 4/3, rel_tol=.001) or
    x[1] >= display.Info().current_h, display.list_modes()))
SCREEN_SIZE = Screen_size(*SCREEN_SIZE)

LETTER_SIZE = int(SCREEN_SIZE.height / 12.8)
MEDIUM_LETTER_SIZE = LETTER_SIZE * 3 // 4
SMALL_LETTER_SIZE = LETTER_SIZE // 2

LETTER_SQUARE_SIZE = (LETTER_SIZE * 4 // 3, ) * 2
SMALL_LETTER_SQUARE_SIZE = (SMALL_LETTER_SIZE // 2, ) * 2
WIDE_BUTTON_SIZE = (LETTER_SQUARE_SIZE[0] * 5 // 2, LETTER_SQUARE_SIZE[0])

BG_COLOR = color.Color('lightblue')
LETTER_BG_COLOR = color.Color('lavender')
LETTER_COLOR = color.Color('darkblue')
REVEAL_LETTER_COLOR = color.Color('darkmagenta')
NEW_LETTER_COLOR = color.Color('magenta3')
#FONT_FILE = '/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf'
