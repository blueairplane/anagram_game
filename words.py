"""
Words module for anagram.py

"""

import os
import random

import pygame

from constants import *

class Words():
    """
    """
    
    loaded_words = {}
    
    def __init__(self, config):
        self.config = config
        try:
            settings = Words.loaded_words[config.word_length]
            self.words = settings['words']
            self.word_lens = settings['word_lens']
        except KeyError:
            self.words = self._load_words()
            self.word_lens = {}
            self.add_word() # start with one word
            Words.loaded_words[config.word_length] = {
                'words': self.words,
                'word_lens': self.word_lens
            }
        
    def _load_words(self):
        """
        Load a list of words of length 'length' from config's dict file.
        """
        words = os.popen('egrep "^%(pattern)s{%(word_length)d}$" %(dict_file)s'
                         % self.config.to_dict()).readlines()
        words = [w.strip() for w in words]
        random.shuffle(words)
        return words
        
    def _get_words_len(self, word):
        """
        Calculate the number of words that can be made from 'word'.
        """
        num = os.popen('an -d %(dict_file)s -wm3 %(word)s |'
                       'grep "^%(pattern)s*$" | sort -u | wc -l'
                        % dict(locals(), **self.config.to_dict())).read()
        return int(num)
        
    def add_word(self):
        """
        Add a new random word to the word length dict.
        """
        word = random.choice(self.words)
        self.word_lens[word] = self._get_words_len(word)


class Word():
    def __init__(self, word):
        self.word = word
        self.visible = False
        self.new = False
        self.xy = (0, 0)
        self.images = {
            'visible': Word.font_render(word),
            'hidden': Word.font_render('_'*len(word)),
            'reveal': Word.font_render(word, REVEAL_LETTER_COLOR),
            'new': Word.font_render(word, NEW_LETTER_COLOR)
        }
    
    def __eq__(self, other):
        return self.word == other
    
    def __len__(self):
        return len(self.word)
        
    def __str__(self):
        return self.word
    
    @staticmethod
    def font_render(text, color=LETTER_COLOR):
        smallfont = pygame.font.Font(None, SMALL_LETTER_SIZE)
        return smallfont.render(text, True, color, BG_COLOR).convert()


class WordList(list):
    """
    """
    
    def __init__(self, word, config, surf):
        self.surf = surf
        n = 0
        word_len = 0
        for w in self._get_words_from(word, config):
            word = Word(w)
            if len(word) > word_len:
                word_len = len(word)
                label = Word('- %d -' % word_len)
                label.visible = True
                label.xy = self._xy(n)
                self.append(label)
                n += 1
            word.xy = self._xy(n)
            self.append(word)
            n += 1
            
    def _get_words_from(self, word, config):
        """
        Create a list of words (>=3 ltrs) that can be made from 'word'.
        """
        
        words = os.popen('an -d %(dict_file)s -wm3 %(word)s | '
                         'grep "^%(pattern)s*$" | sort -u'
                         % dict(locals(), **config.to_dict())).readlines()
        words = [w.strip() for w in words] # remove the newlines
        words.sort(key=len)
        return words
    
    def _xy(self, n):
        ### surf needs to be the whole area we're drawing to, not screen
        ### words_per_col = surf.height / SMALL_LETTER_SIZE (?)
        ### word_height = SMALL_LETTER_SIZE (though, is currently +5)
        ### col_width = (SMALL_LETTER_SIZE * 2 / 3) * len(word)
                
        # ~ xy = lambda x: (80 * (x // 20) + 10, 25 * (x % 20) + 10)
        gap = SMALL_LETTER_SIZE // 3
        col_width = self.surf.get_width() // 10
        row_height = SMALL_LETTER_SIZE
        words_per_col = self.surf.get_height() // SMALL_LETTER_SIZE
        x = col_width * (n // words_per_col) + gap
        y = row_height * (n % words_per_col) + gap
        return (x, y)

            
    def draw(self, reveal):
        """
        Display the word list to 'surf', omitting what has not been found.
        """
        for word in self:
            if word.visible:
                if word.new:
                    self.surf.blit(word.images['new'], word.xy)
                    word.new = False
                else:
                    self.surf.blit(word.images['visible'], word.xy)
            elif reveal:
                self.surf.blit(word.images['reveal'], word.xy)
            else:
                self.surf.blit(word.images['hidden'], word.xy)
