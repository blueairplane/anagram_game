"""
Buttons module for anagram.py

"""

import random

import pygame

from constants import *

class Label(pygame.sprite.Sprite):
    """
    """
    
    def __init__(self, text, size, text_color, bg_color=None, font_name=None):
        super().__init__()
        self.text = text
        self.size = size
        self.text_color = text_color
        self.bg_color = bg_color
        self.font = pygame.font.Font(None, self.size)
        self.image, self.rect = self._render()
        
    def _render(self):
        """
        """
        
        image = self.font.render(self.text, True, self.text_color).convert_alpha()
        # self.bg_color ???
        rect = image.get_rect()
        return image, rect
    
    def set_text(self, text):
        self.text = text
        self._render()

class TextButton(pygame.sprite.Sprite):
    """
    """
    
    def __init__(self, text, size, bg, topleft=(0,0)):
        super().__init__()
        self.bg = bg
        self.rect = self.bg.get_rect()
        self.rect.topleft = topleft
        self.label = Label(text, size, LETTER_COLOR)
        self.label.rect.center = self.rect.center
        self.image = self.bg.copy().convert()
        self.image.blit(self.label.image, self.label.rect)

        
class Letter(TextButton):
    """
    """
    
    ID = 0
    
    def __init__(self, letter, image, size):
        super().__init__(letter, size, image)
        self.visible = True
        self.letter = letter
        self.id = Letter.ID
        Letter.ID += 1


class Guess(TextButton):
    """
    """
    
    def __init__(self, letter):
        super().__init__(
            letter.label.text,
            letter.label.size,
            letter.image
        )
        self.id = letter.id


class LetterButtons(list):
    """
    """
    
    def __init__(self, x, y, gap, buttons):
        if buttons:
            self.extend(buttons)
        self.x = x
        self.y = y
        self.gap = gap
        self.set_positions()
        
    def __str__(self):
        return ''.join((l.label.text for l in self))
        
    def set_positions(self):
        x = self.x
        for button in self:
            button.rect.topleft = (x, self.y)
            x += button.rect.width + self.gap
            if button.visible:
                button.image.set_alpha() # turn off alpha blending
            else:
                button.image.set_alpha(64)
                
    def shuffle(self):
        random.shuffle(self)
        self.set_positions()


class GuessButtons(list):
    """
    """
    
    def __init__(self, x, y, gap):
        self.x = x
        self.y = y
        self.gap = gap
        
    def __str__(self):
        return ''.join((l.label.text for l in self))
                        
    def set_positions(self):
        x = self.x
        for button in self:
            button.rect.topleft = (x, self.y)
            x += button.rect.width + self.gap

