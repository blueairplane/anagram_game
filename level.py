"""
Level module for anagram.py

"""

import sys

from buttons import *
#from config import *
from constants import *
from words import *

class Level():
    """
    """
    
    clock = pygame.time.Clock()
    
    def __init__(self, screen, word_words_len, config, bgs, fonts):
        word, words_len = word_words_len
        self.screen = screen
        self.config = config
        self.bgs = bgs
        self.fonts = fonts
#        self.word = word
        self.words_len = words_len
        self.wordlist = WordList(
            word,
            self.config,
            self.screen.subsurface(
                (0,0),
                (self.screen.get_width(),self.screen.get_height()*2//3)
            )
        )
        self.long_words_len = len([w for w in self.wordlist
                                   if len(w) == self.config.word_length])
        self.correct = 0
        self.long_correct = 0
        
        self.col = (
            SMALL_LETTER_SIZE,
            SCREEN_SIZE.width * 2 // 3,
            SCREEN_SIZE.width * 5 // 6
        )
        self.row = (
            SCREEN_SIZE.height * 7 // 10,
            SCREEN_SIZE.height * 7 // 10 + LETTER_SIZE,
            SCREEN_SIZE.height * 7 // 10 + LETTER_SIZE * 2
        )
        
        self.buttons = LetterButtons(
            self.col[0],
            self.row[0],
            10,
            [Letter(l, bgs["letterbg"], LETTER_SIZE) for l in word]
        )
        self.buttons.shuffle()
        self.guesses = GuessButtons(self.col[0], self.row[2], 0)
        self.submit = TextButton('Submit', LETTER_SIZE, bgs["submitbg"])
        self.submit.rect.topleft = (self.col[1], self.row[2])
        self.scramble = TextButton('Scramble', MEDIUM_LETTER_SIZE, bgs["submitbg"])
        self.scramble.rect.topleft = (self.col[1], self.row[0])
        self.give_up = TextButton('Give up!', MEDIUM_LETTER_SIZE, bgs["submitbg"])
        self.give_up.rect.topleft = (self.col[2], self.row[2])
        self.new_level = TextButton('Next!', LETTER_SIZE, bgs["submitbg"])
        self.new_level.rect.topleft = (self.col[2], self.row[2])
        self.move_on = TextButton('---->', LETTER_SIZE, bgs["submitbg"])
        self.move_on.rect.topleft = (self.col[2], self.row[2])
        
        self.allbuttons = pygame.sprite.RenderPlain(
            self.buttons,
            self.submit,
            self.scramble,
            self.give_up
        )
        
    def _draw(self, reveal):
        """
        """
        
        self.screen.blit(self.bgs["background"], (0, 0))
        self.wordlist.draw(reveal)
        self.allbuttons.draw(self.screen)
        correct_text = self.fonts["medfont"].render(
            '%d/%d' % (self.correct, self.words_len),
            True,
            LETTER_COLOR
        )
        self.screen.blit(correct_text, (self.col[2], self.row[0]))
        long_correct_text = self.fonts["medfont"].render(
            '%d/%d' % (self.long_correct, self.long_words_len),
            True,
            LETTER_COLOR
        )
        self.screen.blit(long_correct_text, (self.col[2], self.row[1]))
        pygame.display.flip()
    
    def play(self):
        """Play this level."""
        
        passed = None
        next_level = False
        quit = False
        
        pygame.event.set_allowed(None)
        pygame.event.set_allowed((pygame.QUIT, pygame.MOUSEBUTTONDOWN, pygame.KEYDOWN))
        
        while not next_level and not quit: # LEVEL
            self._draw(False)
            self.clock.tick(30)
            
            events = [pygame.event.wait()] + pygame.event.get()
            for event in events:
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        guess = str(self.guesses)
                        if guess in self.wordlist:
                            for w in self.wordlist:
                                if not w.visible and w == guess:
                                    w.visible = True
                                    w.new = True
                                    print('You should be getting points now')
                                    break
                    elif event.key == pygame.K_BACKSPACE:
                        if self.guesses:
                            g = self.guesses.pop()
                            for b in self.buttons:
                                if b.id == g.id:
                                    b.visible = True
                                    break
                            g.kill()
                            self.buttons.set_positions()
                            self.guesses.set_positions()
                    else:
                        letter = event.unicode
                        for b in self.buttons:
                            if (b.letter == letter and
                                b.id not in (g.id for g in self.guesses)):
                                new_guess = Guess(b)
                                self.guesses.append(new_guess)
                                self.allbuttons.add(new_guess)
                                b.visible = False
                                self.buttons.set_positions()
                                self.guesses.set_positions()
                                break
                        
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    pos = event.pos
                    if self.submit.rect.collidepoint(pos):
                        guess = str(self.guesses)
                        if guess in self.wordlist:
                            for w in self.wordlist:
                                if not w.visible and w == guess:
                                    w.visible = True
                                    w.new = True
                                    print('You should be getting points now')
                                    break
                    elif self.scramble.rect.collidepoint(pos):
                        self.buttons.shuffle()
                    elif (self.give_up.alive() and
                          self.give_up.rect.collidepoint(pos)):
                        quit = True
                    elif (self.new_level.alive() and
                          self.new_level.rect.collidepoint(pos)):
                        next_level = True
                        print('You should be on the next word/level now')
                    else:
                        for b in self.buttons:
                            if (b.rect.collidepoint(pos) and
                                b.id not in (g.id for g in self.guesses)):
                                new_guess = Guess(b)
                                self.guesses.append(new_guess)
                                self.allbuttons.add(new_guess)
                                b.visible = False
                                self.buttons.set_positions()
                                self.guesses.set_positions()
                        for g in self.guesses:
                            if g.rect.collidepoint(pos):
                                for b in self.buttons:
                                    if b.id == g.id:
                                        b.visible = True
                                        break
                                g.kill()
                                self.guesses.remove(g)
                                self.buttons.set_positions()
                                self.guesses.set_positions()
                            
            self.correct = sum(w.visible for w in self.wordlist
                if not str(w).startswith('-'))
            self.long_correct = sum(w.visible for w in self.wordlist
                if len(w) == self.config.word_length
                and not str(w).startswith('-'))
            passed = self.long_correct == self.long_words_len and \
                100 * self.correct // self.words_len >= self.config.threshold
            if not self.new_level.alive() and passed:
                self.give_up.kill()
                self.new_level.add(self.allbuttons)
            if self.correct == self.words_len: # all correct
                break
        
        self.allbuttons.empty()
        self.move_on.add(self.allbuttons)
        
        self._draw(True)
        
        move_on = True
        while move_on:
            events = pygame.event.get()
            if not events:
                continue
            for event in events:
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    pos = event.pos
                    if self.move_on.rect.collidepoint(pos):
                        move_on = False
                        
        return quit

